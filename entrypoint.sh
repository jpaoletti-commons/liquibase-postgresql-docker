#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

liquibase --url "jdbc:postgresql://$LIQUIBASE_HOST:$LIQUIBASE_PORT/$LIQUIBASE_DATABASE" \
          --driver "org.postgresql.Driver" \
          --username "$LIQUIBASE_USERNAME" \
          --password "$LIQUIBASE_PASSWORD" \
          --changeLogFile "$LIQUIBASE_CHANGELOG" \
          --logLevel "$LIQUIBASE_LOGLEVEL" \
          "$@"