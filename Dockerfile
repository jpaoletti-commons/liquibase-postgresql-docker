FROM openjdk:8-jre-alpine

LABEL maintainer="julien.paoletti@gmail.com"

ARG liquibase_version=3.6.2
ARG pg_jdbc_driver_version=42.2.5

ENV LIQUIBASE_CHANGELOG="master.xml" \
    LIQUIBASE_LOGLEVEL="info" \
    LIQUIBASE_EXPORTDATADIR="/var/liquibase/export"

# update & upgrade & installs bash
# installs liquibase and postgresql JDBC driver
# and adds liquibase user (non root user for security purpose)
RUN apk update && apk upgrade && apk --no-cache add bash \
  && archive=/tmp/liquibase.tar.gz && install_dir=/opt/liquibase \
  && wget -q -O ${archive} "https://github.com/liquibase/liquibase/releases/download/liquibase-parent-${liquibase_version}/liquibase-${liquibase_version}-bin.tar.gz" \
  && mkdir -p ${install_dir} /usr/share/liquibase/model /var/liquibase/export \
  && tar -xzf ${archive} -C ${install_dir} \
  && rm -f ${archive} \
  && chmod +x ${install_dir}/liquibase \
  && ln -s ${install_dir}/liquibase /usr/local/bin/ \
  && wget -q -O ${install_dir}/lib/postgresql.jar "https://jdbc.postgresql.org/download/postgresql-${pg_jdbc_driver_version}.jar" \
  && addgroup -g 1000 -S liquibase && adduser -u 1000 -S liquibase -G liquibase

# FIX for issue with missing slf4j lib with 3.6.x liquibase versions (to remove when issue if fixed in next versions)
RUN [[ ! -f /opt/liquibase/lib/slf4j-api-1.7.25.jar ]] && wget -q -O /opt/liquibase/lib/slf4j-api-1.7.25.jar "http://central.maven.org/maven2/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar"

# non root user for security purpose
USER liquibase

WORKDIR /usr/share/liquibase/model

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]

