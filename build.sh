#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

cd "$(dirname "${BASH_SOURCE[0]}")"

liquibase_version=$(cat Dockerfile | grep liquibase_version | head -n 1 | cut -d '=' -f 2)
image_id="liquibase-postgres"
version_tag="$image_id:$liquibase_version"
latest_tag="$image_id:latest"

# local version
echo -e "Building image \\e[33m$version_tag\\e[39m .."
echo
docker build -t "$version_tag" .
echo

# local latest
echo -e "Tagging image \\e[33m$latest_tag\\e[39m from \\e[33m$version_tag\\e[39m .."
docker tag "$version_tag" "$latest_tag"

echo
docker images "$image_id"

