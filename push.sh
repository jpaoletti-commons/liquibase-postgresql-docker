#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

docker_id="jpaoletti"
image_id="liquibase-postgres"
liquibase_version=$(cat Dockerfile | grep liquibase_version | head -n 1 | cut -d '=' -f 2)

version_tag="$image_id:$liquibase_version"
latest_tag="$image_id:latest"
docker_hub_version_tag="$docker_id/$image_id:$liquibase_version"
docker_hub_latest_tag="$docker_id/$image_id:latest"

# version
echo -e "Tagging image \\e[33m$docker_hub_version_tag\\e[39m from \\e[33m$version_tag\\e[39m .."
docker tag "$version_tag" "$docker_hub_version_tag"
# latest
echo -e "Tagging image \\e[33m$docker_hub_latest_tag\\e[39m from \\e[33m$latest_tag\\e[39m .."
docker tag "$latest_tag" "$docker_hub_latest_tag"
echo
docker images "$docker_id/$image_id"
echo

docker login
echo

echo -e "Pushing image \\e[33m$docker_hub_version_tag\\e[39m .."
docker push "$docker_hub_version_tag"
echo
echo -e "Pushing image \\e[33m$docker_hub_latest_tag\\e[39m .."
docker push "$docker_hub_latest_tag"