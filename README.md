# liquibase-postgresql-docker

A docker container with **Liquibase** ready to be used against latest **PostgreSql** (v10) dbms :

- Liquibase v**3.6.2**
- Postgres JDBC Driver v**42.2.5**

[![Anchore Image Overview](https://anchore.io/service/badges/image/d0a61cab157b9a12dc70801f429c243770633c24ee43c98d6d2e2cd077117f29)](https://anchore.io/image/dockerhub/jpaoletti%2Fliquibase-postgres%3Alatest)

## Usage

```sh
docker run --rm \
  -e LIQUIBASE_HOST=<host> \
  -e LIQUIBASE_PORT=<port> \
  -e LIQUIBASE_USERNAME=<user> \
  -e LIQUIBASE_PASSWORD=<password> \
  -e LIQUIBASE_DATABASE=<database> \
  -v </path/to/liquibase/files>:/usr/share/liquibase/model \
  jpaoletti/liquibase-postgres <liquibase command>
```

Replace any ```<parameter>``` by the corresponding value.